const express=require('express')
const Router=express.Router()
const {check, validationResult}=require('express-validator')
const fs=require('fs')
const softwareDao=require('../models/softwareModel')
const supplierDao=require('../models/supplierModel')
const userDao=require('../models/userModel')
const rand=require('random-key')
const date = require('date-and-time');
const multer=require('multer')
const upload=multer({dest:'uploads', 
fileFilter:(req,file,callback)=>{ 
    if(file.mimetype.startsWith('image/')){ 
        callback(null,true)
    }
    else{
        callback(null,false)
    }
},limits:{fileSize:5000000}})
let imgPath=[]

function numberFormat(n){
    return n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
}

// HOME PAGE
Router.get('/',(req,res)=>{
    const curUser=req.session.user
    if(!curUser){
        return res.redirect('/login')
    }
    const error=req.flash('error') || ''
    softwareDao.find().sort({_id:-1})
    .then(softwares=>{
        return res.render('index',{softwares,error,search:'',curUser})
    })
})

Router.post('/',(req,res)=>{
    const curUser=req.session.user
    if(!curUser){
        return res.redirect('/login')
    }
    let {search,sort}=req.body
    if(!search && sort==='0'){
        req.flash('error','Vui lòng nhập thông tin tìm kiếm')
        return res.redirect('/')
    }
    if(sort==='0'){
        softwareDao.find({softwareName:new RegExp(search,'i')}).sort({_id:-1})
        .then(softwares =>{
            return res.render('index',{softwares,error:'',search,curUser})
        })
    }
    else if(!search){
        let a
        if(sort==='1'){
            a={
                softwareName:1
            }
        }
        else if(sort==='2'){
            a={
                price:-1
            }
        }
        else if(sort==='3'){
            a={
                price:1
            }
        }

        softwareDao.find().sort(a)
        .then(softwares=>{
            return res.render('index',{softwares,error:'',search:'',curUser})
        })
    }
    else{
        let a
        if(sort==='1'){
            a={
                softwareName:1
            }
        }
        else if(sort==='2'){
            a={
                price:-1
            }
        }
        else if(sort==='3'){
            a={
                price:1
            }
        }
        softwareDao.find({softwareName:new RegExp(search,'i')}).sort(a)
        .then(softwares=>{
            return res.render('index',{softwares,error:'',search,curUser})
        })
    }
})

// SOFTWARE MANAGER
Router.get('/softwareManager',(req,res)=>{
    const curUser=req.session.user
    if(!curUser){
        return res.redirect('/login')
    }
    else if(curUser.role==='customer'){
        req.flash('error','Bạn không có quyền truy cập')
        return res.redirect('/')
    }
    const error=req.flash('error') || ''
    const success=req.flash('success') || ''
    softwareDao.find().sort({_id:-1})
    .then(softwares=>{
        res.render('softwareManager',{softwares,error,search:'',curUser,success})
    })
})

Router.post('/softwareManager',(req,res)=>{
    const curUser=req.session.user
    if(!curUser){
        return res.redirect('/login')
    }
    else if(curUser.role==='customer'){
        req.flash('error','Bạn không có quyền truy cập')
        return res.redirect('/')
    }
    let {search,sort}=req.body
    if(!search && sort==='0'){
        req.flash('error','Vui lòng nhập thông tin tìm kiếm')
        return res.redirect('/softwareManager')
    }
    if(sort==='0'){
        softwareDao.find({softwareName:new RegExp(search,'i')}).sort({_id:-1})
        .then(softwares =>{
            return res.render('softwareManager',{softwares,error:'',search,curUser,success:''})
        })
    }
    else if(!search){
        let a
        if(sort==='1'){
            a={
                softwareName:1
            }
        }
        else if(sort==='2'){
            a={
                price:-1
            }
        }
        else if(sort==='3'){
            a={
                price:1
            }
        }

        softwareDao.find().sort(a)
        .then(softwares=>{
            return res.render('softwareManager',{softwares,error:'',search:'',curUser,success:''})
        })
    }
    else{
        let a
        if(sort==='1'){
            a={
                softwareName:1
            }
        }
        else if(sort==='2'){
            a={
                price:-1
            }
        }
        else if(sort==='3'){
            a={
                price:1
            }
        }
        softwareDao.find({softwareName:new RegExp(search,'i')}).sort(a)
        .then(softwares=>{
            return res.render('softwareManager',{softwares,error:'',search,curUser,success:''})
        })
    }
})


Router.get('/software/:id',(req,res)=>{
    const curUser=req.session.user
    if(!curUser){
        return res.redirect('/login')
    }
    const {id}=req.params
    softwareDao.findOne({_id:id})
    .then((software)=>{
        supplierDao.findOne({_id:software.supplierId})
        .then((sup)=>{
            const supplierName=sup.supplierName
            const price=numberFormat(software.price)
            return res.render('software',{software,supplierName,price,curUser})
        })
    })
    .catch((err)=>{
        return res.redirect('/error')
    })
})

Router.put('/software/:id', async(req,res)=>{
    const curUser=req.session.user
    if(!curUser){
        return res.redirect('/login')
    }
    const {id}=req.params
    if(!req.session.cart){
        req.session.cart=[]
    }
    await softwareDao.findOne({_id:id})
    .then(software=>{
        let a={stt:req.session.cart.length,name:software.softwareName,price:software.price}
        req.session.cart.push(a)
    })
    res.redirect('/cart')
})

const addSoftwareValidator=[
    check('name').exists().withMessage('Vui lòng nhập tên sản phẩm')
    .notEmpty().withMessage('Vui lòng nhập tên sản phẩm'),

    check('price').exists().withMessage('Vui lòng nhập giá')
    .notEmpty().withMessage('Vui lòng nhập giá')
    .isNumeric().withMessage('Giá không hợp lệ'),

    check('description').exists().withMessage('Vui lòng nhập mô tả')
    .notEmpty().withMessage('Vui lòng nhập mô tả')
]

Router.get('/addSoftware',(req,res)=>{
    const curUser=req.session.user
    if(!curUser){
        return res.redirect('/login')
    }
    else if(curUser.role==='customer'){
        req.flash('error','Bạn không có quyền truy cập')
        return res.redirect('/')
    }
    const error=req.flash('error') || ''
    const name=req.flash('name') || ''
    const price=req.flash('price') || ''
    const desc=req.flash('desc') || ''
    supplierDao.find()
    .then(sups=>{
        res.render('addsoftware',{error,sups,name,price,desc,curUser})
    })
})

Router.post('/addSoftware',(req,res,next)=>{
    const curUser=req.session.user
    if(!curUser){
        return res.redirect('/login')
    }
    else if(curUser.role==='customer'){
        req.flash('error','Bạn không có quyền truy cập')
        return res.redirect('/')
    }
    let uploader=upload.single('image')
    uploader(req,res,err=>{
        let msg=''
        let software=req.body
        let image=req.file
        if(!image){
            msg+='Vui lòng chọn ảnh sản phẩm'
        }
        else if(err){
            msg+='Kích thước ảnh không hợp lệ'
        }

        if(msg.length>0){
            req.flash('error',msg)
            req.flash('name',software.name)
            req.flash('price',software.price)
            req.flash('desc',software.description)
            return res.redirect('/addSoftware')
        }
        fs.renameSync(image.path,`uploads/${image.originalname}`)
        const img=`uploads/${image.originalname}`
        imgPath.push(img)
        next()
    })
})

Router.post('/addSoftware',addSoftwareValidator,(req,res)=>{
    const curUser=req.session.user
    if(!curUser){
        return res.redirect('/login')
    }
    else if(curUser.role==='customer'){
        req.flash('error','Bạn không có quyền truy cập')
        return res.redirect('/')
    }
    let result=validationResult(req)
    let software=req.body
    const img=imgPath.pop()
    if(result.errors.length===0){
        supplierDao.findOne({_id:software.supplier}).then((sup)=>{
            if(!sup){
                req.flash('error','Không tìm thấy nhà cung cấp')
                return res.redirect('/addSoftware')
            }
        })
        const s= new softwareDao({
            softwareName:software.name,
            price:software.price,
            description:software.description,
            softwareImage:img,
            supplierId:software.supplier
        })
        return s.save().then(()=>{
            req.flash('success','Thêm sản phẩm thành công')
            res.redirect('/softwareManager')
        }) 
    }
    else{
        result=result.mapped()

        let message
        for(fields in result){
            message=result[fields].msg
            break
        }
        req.flash('error',message)
        
        res.redirect('/addSoftware')
    } 
})

Router.get('/editSoftware/:id', function ( req, res ) {
    const curUser=req.session.user
    if(!curUser){
        return res.redirect('/login')
    }
    else if(curUser.role==='customer'){
        req.flash('error','Bạn không có quyền truy cập')
        return res.redirect('/')
    }
    const {id}=req.params
    const error=req.flash('error') || ''
    softwareDao.findOne({_id:id})
    .then((software)=>{
        if(software){  
            return res.render('editSoftware',{software,error,curUser})
        }
        else{
            return res.redirect('/error')
        }
    })
})

const editSoftwareValidator=[
    check('name').exists().withMessage('Vui lòng nhập tên sản phẩm')
    .notEmpty().withMessage('Vui lòng nhập tên sản phẩm'),

    check('price').exists().withMessage('Vui lòng nhập giá')
    .notEmpty().withMessage('Vui lòng nhập giá')
    .isNumeric().withMessage('Giá không hợp lệ'),

    check('description').exists().withMessage('Vui lòng nhập mô tả')
    .notEmpty().withMessage('Vui lòng nhập mô tả')
]

Router.post('/editSoftware/:id',editSoftwareValidator,(req,res)=>{
    const curUser=req.session.user
    if(!curUser){
        return res.redirect('/login')
    }
    else if(curUser.role==='customer'){
        req.flash('error','Bạn không có quyền truy cập')
        return res.redirect('/')
    }
    const {id}=req.params
    let result=validationResult(req)
    software = req.body
    if(result.errors.length===0){
        softwareDao.findByIdAndUpdate(id,{softwareName:software.name,price:software.price,description:software.description},{new:true})
        .then(software=>{
            if(!software){
                req.flash('error','Sản phẩm không tồn tại hoặc đã bị xóa')
                return res.redirect('/softwareManager')
            }
            else{
                req.flash('success','Chỉnh sửa sản phẩm thành công')
                return res.redirect('/softwareManager')
            }
        })
    }
    else{
        result=result.mapped()

        let message
        for(fields in result){
            message=result[fields].msg
            break
        }
        req.flash('error',message)
        
        res.redirect(`/editSoftware/${id}`)
    } 
})

Router.get('/editImage/:id', function ( req, res ) {
    const curUser=req.session.user
    if(!curUser){
        return res.redirect('/login')
    }
    else if(curUser.role==='customer'){
        req.flash('error','Bạn không có quyền truy cập')
        return res.redirect('/')
    }
    const {id}=req.params
    const error=req.flash('error') || ''
    softwareDao.findOne({_id:id})
    .then(software=>{
        if(software){
            return res.render('editImage',{error,software,curUser})
        }
        else{
            return res.redirect('/error')
        }
    })
    
})

Router.post('/editImage/:id',(req,res)=>{
    const curUser=req.session.user
    if(!curUser){
        return res.redirect('/login')
    }
    else if(curUser.role==='customer'){
        req.flash('error','Bạn không có quyền truy cập')
        return res.redirect('/')
    }
    let uploader=upload.single('image')
    let {id}=req.params
    uploader(req,res,err=>{
        let msg=''
        let software=req.body
        let image=req.file
        if(!image){
            msg+='Vui lòng chọn ảnh sản phẩm'
        }
        else if(err){
            msg+='Kích thước ảnh không hợp lệ'
        }

        if(msg.length>0){
            req.flash('error',msg)
            return res.redirect('/editImage/'+id)
        }
        fs.renameSync(image.path,`uploads/${image.originalname}`)
        const img=`uploads/${image.originalname}`

        softwareDao.findByIdAndUpdate(id,{softwareImage:`uploads/${image.originalname}`},{new:true})
        .then(software=>{
            if(!software){
                req.flash('error','Sản phẩm không tồn tại hoặc đã bị xóa')
                return res.redirect('/softwareManager')
            }
            else{
                req.flash('success','Chỉnh sửa ảnh thành công')
                return res.redirect('/softwareManager')
            }
        })
    })
})

Router.delete('/software/:id', function ( req, res ) {
    const curUser=req.session.user
    if(!curUser){
        return res.redirect('/login')
    }
    else if(curUser.role==='customer'){
        req.flash('error','Bạn không có quyền truy cập')
        return res.redirect('/')
    }
    let {id}=req.params
    softwareDao.findByIdAndDelete(id)
    .then(s=>{
        if(s){
            req.flash('success','Xóa sản phẩm thành công')
            return res.redirect('/softwareManager')
        }
        req.flash('error','Sản phẩm không tồn tại')
        return res.redirect('/softwareManager')
        
    })
})

// -------------------------------------------------------------------------------



Router.get('/cart',(req,res)=>{
    const curUser=req.session.user
    if(!curUser){
        return res.redirect('/login')
    }
    let cart=req.session.cart
    const error=req.flash('error') || ''
    sum=0
    if(cart){ 
        cart.forEach(s=>{
            sum+=s.price
        })
        cash=numberFormat(sum) || 0
        return res.render('cart',{cart,cash,curUser,error})
    }
    return res.render('cart',{cart,cash:0,curUser,error})
})

Router.put('/cart',async(req,res)=>{
    const curUser=req.session.user
    if(!curUser){
        return res.redirect('/login')
    }
    const now = new Date()
    let a= date.format(now,'DD/MM/YYYY')
    let cart=req.session.cart
    let index=0
    let obj
    if(!cart){
        req.flash('error','Vui lòng thêm sản phẩm')
        return res.redirect('/cart')
    }
    await cart.forEach(s=>{
        index+=1
    })
    obj=``
    await cart.forEach(sof=>{
        let b= rand.generate(11)
        obj+= `{"softwareName":"${sof.name}","buyDate":"${a}","key":"${b}"},`
    })
    obj = obj.substring(0, obj.length - 1)
    if(index>=2){
        obj=`[${obj}]`
    }
    console.log(obj)
    userDao.findOneAndUpdate({_id:curUser._id},{$push:{mySoftware:JSON.parse(obj)}},{upsert: true})
    .then((result)=>{
        console.log('ok')
    })
    delete req.session.cart
    req.flash('success','Thanh toán thành công')
    return res.redirect('/history')
})

Router.delete('/cart/:id', async(req,res)=>{
    const curUser=req.session.user
    if(!curUser){
        return res.redirect('/login')
    }
    const{id}=req.params
    let cart=req.session.cart
    index=0
    await cart.forEach(s=>{
        a=parseInt(id)
        if(s.stt===a){
            cart.splice(index,1)
        }
        else{
            index+=1
        }
    })
    return res.redirect('/cart')
})



// SUPPLIER MANAGER
Router.get('/supplierManager',(req,res)=>{
    const curUser=req.session.user
    if(!curUser){
        return res.redirect('/login')
    }
    else if(curUser.role==='customer'){
        req.flash('error','Bạn không có quyền truy cập')
        return res.redirect('/')
    }
    const error=req.flash('error') || ''
    const success=req.flash('success') || ''
    supplierDao.find().sort({_id:-1})
    .then(suppliers=>{
        res.render('supplierManager',{suppliers,error,curUser,search:'',success})
    })
})

Router.post('/supplierManager',(req,res)=>{
    const curUser=req.session.user
    if(!curUser){
        return res.redirect('/login')
    }
    else if(curUser.role==='customer'){
        req.flash('error','Bạn không có quyền truy cập')
        return res.redirect('/')
    }
    let {search}=req.body
    if(!search){
        req.flash('error','Vui lòng nhập thông tin tìm kiếm')
        return res.redirect('/supplierManager')
    }
    supplierDao.find({$or:[{supplierName:new RegExp(search,'i')},{supplierEmail:search}]}).sort({supplierName:1})
        .then(suppliers=>{
            return res.render('supplierManager',{suppliers,error:'',search,curUser,success:''})
        })
})

Router.get('/addSupplier',(req,res)=>{
    const curUser=req.session.user
    if(!curUser){
        return res.redirect('/login')
    }
    else if(curUser.role==='customer'){
        req.flash('error','Bạn không có quyền truy cập')
        return res.redirect('/')
    }
    const error=req.flash('error')
    res.render('addSupplier',{error,curUser})
})

const addSupplierValidator=[
    check('name').exists().withMessage('Vui lòng nhập tên nhà cung cấp')
    .notEmpty().withMessage('Vui lòng nhập tên nhà cung cấp'),

    check('email').exists().withMessage('Vui lòng nhập email nhà cung cấp')
    .notEmpty().withMessage('Vui lòng nhập email nhà cung cấp')
    .isEmail().withMessage('Email sai định dạng')
]

Router.post('/addSupplier',addSupplierValidator,(req,res)=>{
    const curUser=req.session.user
    if(!curUser){
        return res.redirect('/login')
    }
    else if(curUser.role==='customer'){
        req.flash('error','Bạn không có quyền truy cập')
        return res.redirect('/')
    }
    let result=validationResult(req)
    let {name,email}=req.body
    if(result.errors.length===0){
        const sup= new supplierDao({
            supplierName:name,
            supplierEmail:email
        })
        return sup.save().then(()=>{
            req.flash('success','Thêm nhà cung cấp thành công')
            res.redirect('/supplierManager')
        }) 
        .catch(err=>{
            req.flash('error','Email đã tồn tại')
            return res.redirect('/addSupplier')
        }) 
    }
    else{
        result=result.mapped()

        let message
        for(fields in result){
            message=result[fields].msg
            break
        }
        req.flash('error',message)
        
        res.redirect('/addSupplier')
    } 
})

Router.get('/supplier/:id',(req,res)=>{
    const curUser=req.session.user
    if(!curUser){
        return res.redirect('/login')
    }
    else if(curUser.role==='customer'){
        req.flash('error','Bạn không có quyền truy cập')
        return res.redirect('/')
    }
    const {id}=req.params
    supplierDao.findOne({_id:id})
    .then((profile)=>{
        if(profile){
            softwareDao.find({supplierId:profile._id})
            .then((softwares)=>{
                return res.render('supplier',{profile,softwares,curUser})
            })
        }
        else{
            return res.redirect('/error') 
        } 
    })
})

Router.delete('/supplier/:id', function ( req, res ) {
    const curUser=req.session.user
    if(!curUser){
        return res.redirect('/login')
    }
    else if(curUser.role==='customer'){
        req.flash('error','Bạn không có quyền truy cập')
        return res.redirect('/')
    }
    let {id}=req.params
    supplierDao.findById(id)
    .then(s=>{
        softwareDao.deleteMany({supplierId:s._id})
    })
    supplierDao.findByIdAndDelete(id)
    .then(s=>{
        if(s){
            req.flash('success','Xóa nhà cung cấp thành công')
            return res.redirect('/supplierManager')
        }
        req.flash('error','Nhà cung cấp không tồn tại')
        return res.redirect('/supplierManager')
        
    })
})
// ----------------------------------------------------
module.exports=Router