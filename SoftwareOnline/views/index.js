const express=require('express')
const app=express()
const dotenv = require('dotenv').config() 
const cookieParser=require('cookie-parser')
const session=require('express-session')
const flash= require('express-flash')
const UserRouter=require('./Routes/UserRouter')
const SoftwareRouter=require('./Routes/SoftwareRouter')
const mongoose=require('mongoose')

app.set('view engine','ejs')
app.use(express.urlencoded({extended:false}))
app.use(express.static(__dirname))
app.use(cookieParser('mk'))
app.use(session({
    secret: process.env.SESSION_SECRET
}))
app.use(express.json())
app.use(flash())

app.use('/',UserRouter)
app.use('/',SoftwareRouter)

app.get('/dashboard',(req,res)=>{
    res.render('dashboard')
})

app.get('/error',(req,res)=>{
    res.render('error')
})


app.use((req,res)=>{
    res.redirect('/error')
})

mongoose.connect("mongodb://localhost/SoftwareOnline",{
    useNewUrlParser:true,
    useUnifiedTopology:true
})
.then(()=>{
    const port=process.env.PORT || 8080
    app.listen(port,()=>console.log(`http://localhost:${port}`))
})
.catch(e=>console.log("Khong the ket noi"))