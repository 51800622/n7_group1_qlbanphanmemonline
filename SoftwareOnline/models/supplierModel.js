const mongoose = require('mongoose')
const Schema = mongoose.Schema

const SupplierSchema=new Schema({
    supplierName:String,
    supplierEmail:{
        type:String,
        unique: true
    }
})

module.exports=mongoose.model('Supplier',SupplierSchema)