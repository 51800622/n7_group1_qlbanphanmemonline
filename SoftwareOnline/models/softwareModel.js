const mongoose = require('mongoose')
const Schema = mongoose.Schema,
ObjectId = Schema.ObjectId
const SoftwareSchema=new Schema({
    softwareName:String,
    price:Number,
    description:String,
    softwareImage:String,
    supplierId:ObjectId
})
module.exports=mongoose.model('Software',SoftwareSchema)