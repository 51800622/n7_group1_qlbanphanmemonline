const mongoose = require('mongoose')
const Schema = mongoose.Schema

const UserSchema=new Schema({
    email:{
        type:String,
        unique: true,
    },
    ho:String,
    ten:String,
    avatar:String,
    ngaysinh:String,
    bankId:String,
    bankName:String,
    password:String,
    role:String,
    mySoftware:[
        {
            softwareName:String,
            buyDate:String,
            key:String
        }
    ]
})

module.exports=mongoose.model('User',UserSchema)